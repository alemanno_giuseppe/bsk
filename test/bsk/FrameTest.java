package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	Frame frame;
	
	@Test
	public void testFrameWithPinsKnockedDownBetweenZeroAndTen() throws Exception{
		int firstThrow = 4;
		int secondThrow = 2;
		frame = new Frame(firstThrow,secondThrow);
		assertEquals("Assert of first Score",4,frame.getFirstThrow());
		assertEquals("Assert of second Score",2,frame.getSecondThrow());
		
	}
		
	@Test (expected=BowlingException.class)
	public void testFrameWithFirstThrowHavingPinsKnockedDownUnderZero() throws Exception{
		int firstThrow = -4;
		int secondThrow = 2;
		frame = new Frame(firstThrow,secondThrow);
	}
	
	@Test (expected=BowlingException.class)
	public void testFrameWithSecondThrowHavingPinsKnockedDownUnderZero() throws Exception{
		int firstThrow = 4;
		int secondThrow = -2;
		frame = new Frame(firstThrow,secondThrow);
	}
	
	@Test (expected=BowlingException.class)
	public void testFrameWithFirstThrowHavingPinsKnockedDownOverTen() throws Exception{
		int firstThrow = 11;
		int secondThrow = 5;
		frame = new Frame(firstThrow,secondThrow);
	}
	
	@Test (expected=BowlingException.class)
	public void testFrameWithSecondThrowHavingPinsKnockedDownOverTen() throws Exception{
		int firstThrow = 4;
		int secondThrow = 11;
		frame = new Frame(firstThrow,secondThrow);
	}
	
	@Test (expected=BowlingException.class)
	public void testFrameWithSumOfPinsKnockedDownOverTen() throws Exception{
		int firstThrow = 9;
		int secondThrow = 9;
		frame = new Frame(firstThrow,secondThrow);
	}
	
	@Test
	public void testGetScore() throws Exception{
		int firstThrow = 2;
		int secondThrow = 6;
		frame = new Frame(firstThrow,secondThrow);
		assertEquals(8,frame.getScore());
	}
	
	@Test
	public void testFrameIsSpare() throws Exception{
		int firstThrow = 4;
		int secondThrow = 6;
		frame = new Frame(firstThrow,secondThrow);
		assertEquals(true,frame.isSpare());
	}
	
	@Test
	public void testFrameIsNotSpare() throws Exception{
		int firstThrow = 4;
		int secondThrow = 4;
		frame = new Frame(firstThrow,secondThrow);
		assertNotEquals(true,frame.isSpare());
	}
	
	@Test
	public void testFrameIsStrike() throws Exception{
		int firstThrow = 10;
		int secondThrow = 0;
		frame = new Frame(firstThrow,secondThrow);
		assertEquals(true,frame.isStrike());
	}
	
	@Test
	public void testFrameIsNotStrike() throws Exception{
		int firstThrow = 6;
		int secondThrow = 0;
		frame = new Frame(firstThrow,secondThrow);
		assertNotEquals(true,frame.isStrike());
	}

}
